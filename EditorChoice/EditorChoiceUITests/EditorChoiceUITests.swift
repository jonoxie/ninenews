//
//  EditorChoiceUITests.swift
//  EditorChoiceUITests
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import XCTest

class EditorChoiceUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }

    override func tearDown() {
        app = nil
        super.tearDown()
        
    }
    
    
    // Testing the UI usability
    func testListPostsToPostDetail() {
        
        // Displaying ListPosts
        XCTAssertEqual(app.navigationBars.element.identifier, "NINE News")
        // Getting post data
        XCTAssert(app/*@START_MENU_TOKEN@*/.tables["ListPostsTable"]/*[[".otherElements[\"listPosts\"].tables[\"ListPostsTable\"]",".tables[\"ListPostsTable\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.cells.count > 0)
        // Tap on the first cell of the table
        app/*@START_MENU_TOKEN@*/.tables["ListPostsTable"]/*[[".otherElements[\"listPosts\"].tables[\"ListPostsTable\"]",".tables[\"ListPostsTable\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.cells["postCell_0"].tap()
        
        // Displaying post detail screen
        XCTAssertEqual(app.navigationBars.element.identifier, "ARTICLE")
        
        sleep(5)
        
        // Find the webview
        let webViewQury:XCUIElementQuery = app.descendants(matching: .webView)
        let webView = webViewQury.element(boundBy: 0)
        XCTAssertNotNil(webView)
        
        // Web contents loaded
        webView.swipeUp()
        
        // Navigate back to the posts screen
        app.navigationBars["ARTICLE"].buttons["NINE News"].tap()
        sleep(1)
        
        // Scroll down and see the cached images worked
        XCTAssertEqual(app.navigationBars.element.identifier, "NINE News")
        app/*@START_MENU_TOKEN@*/.tables["ListPostsTable"]/*[[".otherElements[\"listPosts\"].tables[\"ListPostsTable\"]",".tables[\"ListPostsTable\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        app/*@START_MENU_TOKEN@*/.tables["ListPostsTable"]/*[[".otherElements[\"listPosts\"].tables[\"ListPostsTable\"]",".tables[\"ListPostsTable\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        
        sleep(5)
    }

}
