//
//  EditorChoiceTests.swift
//  EditorChoiceTests
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import XCTest
@testable import EditorChoice

class EditorChoiceTests: XCTestCase {
    
    var sessionUnderTest: URLSession!

    override func setUp() {
        super.setUp()
        sessionUnderTest = URLSession(configuration: URLSessionConfiguration.default)
    }

    override func tearDown() {
        sessionUnderTest = nil
        super.tearDown()
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    // Test correctly formatted data
    func testPostJSONMappingFormatted() throws {
        // 1. Given
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "test_post_formatted", withExtension: "json") else {
            XCTFail("Missing mock json file")
            return
        }
        
        // 2. When
        let jsonData = try Data(contentsOf: url)
        let decoder = JSONDecoder()
        let posts = try decoder.decode(Post.self, from: jsonData)
        
        // 3. Then
        XCTAssertEqual(posts.id, 67184313)
        XCTAssertEqual(posts.assets.count, 3)
    }
    
    // Test missing data
    func testPostJSONMappingMalFormatted() throws {
        // 1. Given
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "test_post_malformatted", withExtension: "json") else {
            XCTFail("Missing mock json file")
            return
        }
        
        // 2. When
        let jsonData = try Data(contentsOf: url)
        let decoder = JSONDecoder()
        let posts = try decoder.decode(Post.self, from: jsonData)
        
        // 3. Then
        XCTAssertEqual(posts.assets.count, 0) // assets missed fallback to default (0)
        XCTAssertEqual(posts.displayName, "") // displayName missed fallback to default ("")
    }
    
    // Test service call over the network
    func testPostServiceWorker() {
        
        // 1. Given
        let productsWorker = ServiceWorker(service: PostService())
        let promise = expectation(description: "Service Worker got the data")
        
        // 2. When
        productsWorker.fetch {
            guard let value = $0.value?.assets, $0.isSuccess else {
                return XCTFail("Server error")
            }
            // 3. Then
            XCTAssertTrue(value.count>0, "have one or more assets")
            
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
    }
    
    // Test the view model data parsing
    func testParsingViewModelData() {
        // 1. Given
        let productsWorker = ServiceWorker(service: PostService())
        let presenter = ListPostsPresenter(viewController: nil)
        let promise = expectation(description: "Service Worker got the data")
        
        // 2. When
        productsWorker.fetch {
            guard let value = $0.value?.assets, $0.isSuccess else {
                return XCTFail("Server error")
            }
            // 3. Then
            let posts = presenter.parsingViewModelData(for: ListPostsModels.Response(posts: value))
            XCTAssertTrue(value.count>0, "have one or more assets")
            XCTAssertTrue(posts.count>0, "parsed")
            XCTAssertNotNil(posts[0].timeAgo)
            
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

}
