//
//  ShowPostDetailViewController.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import UIKit
import WebKit

class ShowPostDetailViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    public var requestURL: URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "ARTICLE"
        
        guard let url = requestURL else { return }
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        webView.navigationDelegate = self
    }
    
    func setupAccessbility() {
        view.accessibilityIdentifier = "postDetail"
        webView.accessibilityIdentifier = "webview"
    }
    
    func showActivityIndicator(show: Bool) {
        if show {
            activity.startAnimating()
        } else {
            activity.stopAnimating()
        }
    }
}

extension ShowPostDetailViewController: WKNavigationDelegate, WKUIDelegate {
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        showActivityIndicator(show: false)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showActivityIndicator(show: true)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        showActivityIndicator(show: false)
    }
}
