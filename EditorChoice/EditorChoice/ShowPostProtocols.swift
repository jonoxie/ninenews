//
//  ShowPostProtocols.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import Foundation

/// ViewController
protocol ShowPostDetailDisplayable: class {
    func displayPostDetail(with url: URL)
    func display(error: ServiceError)
}
