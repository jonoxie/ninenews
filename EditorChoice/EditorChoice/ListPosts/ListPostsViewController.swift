//
//  ListPostsViewController.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import UIKit

class ListPostsViewController: UIViewController {

    @IBOutlet weak fileprivate var tableView: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    lazy private var interactor = ListPostsInteractor(presenter: ListPostsPresenter(viewController:self))
    lazy private var router = ListPostsRouter(viewController: self)
    fileprivate var viewModel: ListPostsModels.ViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "NINE News"
        // hide table while loading data
        tableView.isHidden = true
        // Accessbility
        setupAccessbility()
        // Fetch data
        showActivityIndicator(show: true)
        interactor.fetchPosts(with: ListPostsModels.FetchRequest())
    }
    
    func setupAccessbility() {
        view.accessibilityIdentifier = "listPosts"
        tableView.accessibilityIdentifier = "ListPostsTable"
    }
    
    func showActivityIndicator(show: Bool) {
        if show {
            activity.startAnimating()
        } else {
            activity.stopAnimating()
        }
    }
}

extension ListPostsViewController: ListPostsDisplayable {
    // handle data
    func displayFetchedPosts(with viewModel: ListPostsModels.ViewModel) {
        self.viewModel = viewModel
        
        showActivityIndicator(show: false)
        tableView.isHidden = false
        
        self.tableView.reloadData()
    }
    // handle error
    func display(error: ServiceError) {
        showActivityIndicator(show: false)
        
        let alertController = UIAlertController(
            title: "",
            message: error.message,
            preferredStyle: .alert
        )
        
        alertController.addAction(
            UIAlertAction(title: "OK", style: .default, handler: nil)
        )
        
        present(alertController, animated: true, completion: nil)
    }
}

extension ListPostsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = viewModel?.posts else {
            return 0
        }
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListPostsTableViewCell", for: indexPath) as! ListPostsTableViewCell
        cell.accessibilityIdentifier = "postCell_\(indexPath.row)"
        cell.separatorInset = .zero
        cell.update(post: viewModel?.posts[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let url = viewModel?.posts[indexPath.row].url else { return }
        router.showPostDetail(with: url)
    }
}
