//
//  ListPostsRouter.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import Foundation
import UIKit

struct ListPostsRouter: ListPostsRoutable {
    ///
    weak var viewController: UIViewController?
    
    init(viewController: UIViewController?) {
        self.viewController = viewController
    }
}

extension ListPostsRouter {
    
    func showPostDetail(with url:URL) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "ShowPostDetailViewController") as? ShowPostDetailViewController  else {
            return
        }
        controller.requestURL = url
        
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
