//
//  ListPostProtocols.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import Foundation

/// Router
protocol ListPostsRoutable {
    func showPostDetail(with url:URL)
}

/// Interactor
protocol ListPostsInteractable {
    func fetchPosts(with request: ListPostsModels.FetchRequest)
}

/// Presenter
protocol ListPostsPresentable {
    func presentFetchedPosts(for response: ListPostsModels.Response)
    func presentFetchedPosts(error: ServiceError)
}

/// ViewController
protocol ListPostsDisplayable: class {
    func displayFetchedPosts(with viewModel: ListPostsModels.ViewModel)
    func display(error: ServiceError)
}
