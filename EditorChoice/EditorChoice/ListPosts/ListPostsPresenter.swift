//
//  ListPostsPresenter.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import Foundation

class ListPostsPresenter {
    private weak var viewController: ListPostsDisplayable?
    
    init(viewController: ListPostsDisplayable?) {
        self.viewController = viewController
    }
}

extension ListPostsPresenter: ListPostsPresentable {
    // Present data back to view with formatted data
    func presentFetchedPosts(for response: ListPostsModels.Response) {
        let posts = parsingViewModelData(for: response)
        let viewModel = ListPostsModels.ViewModel(posts:posts)
        viewController?.displayFetchedPosts(with: viewModel)
    }
    // Present error back to view
    func presentFetchedPosts(error: ServiceError) {
        viewController?.display(error: error)
    }
    
    
}

extension ListPostsPresenter {
    
    /// find the smallest image for thumbnail
    fileprivate func selectThumbnailImage(from relatedImages: [RelatedImage]) -> URL? {
        if relatedImages.count > 0 {
            if let smallImage = relatedImages.min(by: { $0.width < $1.width }) {
                return smallImage.url
            }
        }
        return nil
    }
    
    /// convert raw data to view model object (e.g. formatting etc)
    public func parsingViewModelData(for response: ListPostsModels.Response) -> [ListPostsModels.ListPostsViewModel] {
        var posts = response.posts.map {
            ListPostsModels.ListPostsViewModel(
                headline: $0.headline,
                theAbstract: $0.theAbstract,
                byLine: "By "+$0.byLine,
                timeAgo: $0.timeStamp.showTimeAgo(),
                relatedImage: selectThumbnailImage(from: $0.relatedImages),
                timeStamp: $0.timeStamp,
                url: $0.url
            )
        }
        // sorted by timestamp (top down)
        posts.sort{ $0.timeStamp > $1.timeStamp }
        
        return posts
    }
}

