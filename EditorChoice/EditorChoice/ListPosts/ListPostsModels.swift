//
//  ListPostsModels.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import Foundation

// view model structures

enum ListPostsModels {
    struct FetchRequest {
    }
    
    struct Response {
        let posts: [Assets]
    }
    
    struct ViewModel {
        let posts: [ListPostsViewModel]
    }
    
    struct ListPostsViewModel {
        let headline: String
        let theAbstract: String
        let byLine: String
        let timeAgo: String
        let relatedImage: URL?
        let timeStamp: Date
        let url: URL
    }
}
