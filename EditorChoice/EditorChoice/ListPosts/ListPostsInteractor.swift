//
//  ListPostsInteractor.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import Foundation

struct ListPostsInteractor {
    private let presenter: ListPostsPresentable
    private let productsWorker: ServiceWorker
    
    init(presenter: ListPostsPresentable) {
        self.presenter = presenter
        self.productsWorker = ServiceWorker(service: PostService()) // inject the servie so testable
    }
}

extension ListPostsInteractor: ListPostsInteractable {
    func fetchPosts(with request: ListPostsModels.FetchRequest) {
        productsWorker.fetch {
            guard let value = $0.value?.assets, $0.isSuccess else {
                return self.presenter.presentFetchedPosts(error: $0.error ?? .genericError("unknown error"))
            }
            self.presenter.presentFetchedPosts(for: ListPostsModels.Response(posts: value))
        }
    }
}

