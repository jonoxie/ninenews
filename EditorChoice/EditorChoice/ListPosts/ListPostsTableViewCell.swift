//
//  ListPostsTableViewCell.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import UIKit

class ListPostsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var relatedImageView: UIImageView!
    @IBOutlet weak var timeAgoLabel: UILabel!
    
    func update(post: ListPostsModels.ListPostsViewModel?) {
        guard let post = post else { return }
        titleLabel.text = post.headline
        subtitleLabel.text = post.byLine
        timeAgoLabel.text = post.timeAgo
        /// load new image or cached image
        if let imageURLString = post.relatedImage?.absoluteString {
            relatedImageView.loadImage(withCache: imageURLString)
        }
    }

}
