//
//  ServiceError.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import Foundation

/// Service errors
public enum ServiceError: Error {
    case dataIsNotEncodable(String)
    case stringFailedToDecode(String)
    case invalidURL(String)
    case notAuthorised(String)
    case noResponse(String)
    case missingEndpoint(String)
    case failedToParseData(String)
    case missingData(String)
    case failedToDecode(String)
    case genericError(String)
    
    public var message: String {
        switch self {
        case .dataIsNotEncodable(let message),
             .stringFailedToDecode(let message),
             .invalidURL(let message),
             .notAuthorised(let message),
             .noResponse(let message),
             .missingEndpoint(let message),
             .failedToParseData(let message),
             .missingData(let message),
             .failedToDecode(let message),
             .genericError(let message):
            
            return message
        }
    }
}
