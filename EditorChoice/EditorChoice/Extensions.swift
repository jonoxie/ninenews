//
//  Extensions.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import Foundation
import UIKit

let imageCache = NSCache<NSString, UIImage>()

/// Simple image cache using NSCache
extension UIImageView {
    func loadImage(withCache urlString : String) {
        let url = URL(string: urlString)
        if url == nil { return }
        self.image = nil
        
        // check has cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString)  {
            self.image = cachedImage
            return
        }
        
        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView.init(style: .gray)
        addSubview(activityIndicator)
        activityIndicator.startAnimating()
        activityIndicator.center = self.center
        
        // if not, download image from url
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            // main thread
            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    imageCache.setObject(image, forKey: urlString as NSString)
                    self.image = image
                    activityIndicator.removeFromSuperview()
                }
            }
            
        }).resume()
    }
}

extension Date {
    /// Simple date ago function
    func showTimeAgo() -> String {
        let timeStamp = self.timeIntervalSince1970 / 1000
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        return date.timeAgoDisplay()
    }
    
    func timeAgoDisplay() -> String {
        let secondsAgo = Int(Date().timeIntervalSince(self))
        
        if secondsAgo < 60 {
            return "\(secondsAgo) seconds ago"
        }
        else if secondsAgo < 60 * 60 {
            return "\(secondsAgo / 60) minutes ago"
        }
        else if secondsAgo < 60 * 60 * 24 {
            return "\(secondsAgo / 60 / 60) hours ago"
        }
        else if secondsAgo < 60 * 60 * 24 * 7{
            return "\(secondsAgo / 60 / 60 / 24 ) days ago"
        }
        return "\(secondsAgo / 60 / 60 / 24 / 7) weeks ago"
    }
}

