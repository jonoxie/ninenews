//
//  PostService.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import Foundation

// Use enum just for Demo purpose. should be objects or plist
enum PostRequestType {
    static let scheme = "https"
    static let host   = "bruce-v2-mob.fairfaxmedia.com.au"
    static let path   = "/1/coding_test/13ZZQX/full"
    static let method = "GET"
}

public class PostService: ServiceWorkerType {
    init() {}
}

extension PostService {
    public func fetch(completion: @escaping fetchCompletion) {
        ///1. URLComponent object
        var component = URLComponents()
        component.scheme = PostRequestType.scheme
        component.host = PostRequestType.host
        component.path = PostRequestType.path
        guard let url = component.url else {
            completion(.failure(.invalidURL("invalid URL")))
            return
        }
        
        ///2. URLRequest
        var request = URLRequest(url: url)
        request.httpMethod = PostRequestType.method
        
        ///3. URLSession with configuration
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        
        ///4. URLSession task
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            ///5. Make sure handle parsing process in background threat
            DispatchQueue.main.async {
                //a. check error
                guard responseError == nil else {
                    completion(.failure(.noResponse("No response")))
                    return
                }
                //b. check json data
                guard let jsonData = responseData else {
                    completion(.failure(.missingData("Missing data")))
                    return
                }
                //c. decode json
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                decoder.dateDecodingStrategy = .secondsSince1970
                do {
                    let posts = try decoder.decode(Post.self, from: jsonData)
                    completion(.success(posts))
                } catch {
                    completion(.failure(.dataIsNotEncodable("data is not encodable")))
                }
                
            }
        }
        task.resume()
    }
}
