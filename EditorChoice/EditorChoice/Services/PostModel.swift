//
//  PostModel.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import Foundation

public class Post: Codable {
    let id: Int
    let displayName: String
    let assets: [Assets]
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.displayName = try container.decodeIfPresent(String.self, forKey: .displayName) ?? ""
        self.assets = try container.decodeIfPresent([Assets].self, forKey: .assets) ?? []
    }
}

public class Assets: Codable {
    let id: Int
    let headline: String
    let theAbstract: String
    let byLine: String
    let timeStamp: Date
    let relatedImages: [RelatedImage]
    let url: URL
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.headline = try container.decodeIfPresent(String.self, forKey: .headline) ?? ""
        self.theAbstract = try container.decodeIfPresent(String.self, forKey: .theAbstract) ?? ""
        self.byLine = try container.decodeIfPresent(String.self, forKey: .byLine) ?? ""
        self.timeStamp = try container.decodeIfPresent(Date.self, forKey: .timeStamp) ?? Date()
        self.relatedImages = try container.decodeIfPresent([RelatedImage].self, forKey: .relatedImages) ?? []
        self.url = try container.decodeIfPresent(URL.self, forKey: .url) ?? URL(string: "")!
    }
}

public class RelatedImage: Codable {
    let url: URL
    let width: Double
    let height: Double
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.url = try container.decodeIfPresent(URL.self, forKey: .url) ?? URL(string: "")!
        self.width = try container.decodeIfPresent(Double.self, forKey: .width) ?? 0
        self.height = try container.decodeIfPresent(Double.self, forKey: .height) ?? 0
    }
}

