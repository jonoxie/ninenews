//
//  Service.swift
//  EditorChoice
//
//  Created by Jonathan Xie on 11/5/19.
//  Copyright © 2019 NINE. All rights reserved.
//

import Foundation

public typealias fetchCompletion = (Result<Post, ServiceError>) -> Void

/// Protocols
public protocol Service {
    func fetch(completion: @escaping fetchCompletion)
}
public protocol ServiceWorkerType: Service {}

/// Implementation
public struct ServiceWorker: ServiceWorkerType {
    private let service: Service
    
    public init(service: Service) {
        self.service = service
    }
}

extension ServiceWorker {
    public func fetch(completion: @escaping fetchCompletion) {
        service.fetch(completion: completion)
    }
}
